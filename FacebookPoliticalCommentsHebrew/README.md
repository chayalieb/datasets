# datasets


## How to read it 

df = pd.read_csv(FILENAME,delimiter="~",quotechar='^',index_col='comment_id')


## credit
don't forget give me reference for the original papers 

`@inproceedings{liebeskind2019emoji,
  title={Emoji prediction for Hebrew political domain},  
  author={Liebeskind, Chaya and Liebeskind, Shmuel},
  booktitle={Companion Proceedings of The 2019 World Wide Web Conference},
  pages={468--477},
  year={2019}
}
`


`@article{liebeskind2017comparing,
  title={Comparing sentiment analysis models to classify attitudes of political comments on facebook (november 2016)},
  author={Liebeskind, Chaya and Nahon, Karine and HaCohen-Kerner, Yaakov and Manor, Yotam},
  journal={Polibits},
  volume={55},
  pages={17--23},
  year={2017}
}`

`@inproceedings{liebeskind2017comment,
  title={Comment relevance classification in Facebook},
  author={Liebeskind, Chaya and Liebeskind, Shmuel and HaCohen-Kerner, Yaakov},
  booktitle={International Conference on Computational Linguistics and Intelligent Text Processing},
  pages={241--254},
  year={2017},
  organization={Springer}
}`

`@inproceedings{liebeskind2018identifying,
  title={Identifying abusive comments in hebrew facebook},
  author={Liebeskind, Chaya and Liebeskind, Shmuel},
  booktitle={2018 IEEE International Conference on the Science of Electrical Engineering in Israel (ICSEE)},
  pages={1--5},
  year={2018},
  organization={IEEE}
}`

`@inproceedings{liebeskind2017challenges,
  title={Challenges in applying machine learning methods: Studying political interactions on social networks},
  author={Liebeskind, Chaya and Nahon, Karine},
  booktitle={Semanitic Keyword-based Search on Structured Data Sources},
  pages={136--141},
  year={2017},
  organization={Springer}
}`

`@article{liebeskind2019emoji,
  title={Emoji Identification and Prediction in Hebrew Political Corpus.},
  author={Liebeskind, Chaya},
  journal={Issues in Informing Science \& Information Technology},
  number={16},
  year={2019}
}`












